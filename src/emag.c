// emag.c
// Contains functions to control the electro magnets

#include "emag.h"
#include "common.h"

#define LEFT_MAGNET 0
#define RIGHT_MAGNET 1

void emag_leftOn(){
    _turnOn(LEFT_MAGNET);
}

void emag_leftOff(){
    _turnOff(LEFT_MAGNET);
}

void emag_rightOn(){
    _turnOn(RIGHT_MAGNET);
}

void emag_rightOff(){
    _turnOff(RIGHT_MAGNET);
}

/*****************************
 * Helper Functions (private)
 *****************************/

void _turnOn(int magnet){
    // Pull low for on
    set_digital_value(magnet, 0);
}

void _turnOff(int magnet){
    // Pull up for off
    set_digital_value(magnet, 1);
}


