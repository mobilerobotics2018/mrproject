// tracking.c
// Provides functionality to locate the robot and its relative position

#include "tracking.h"
#include "machine.h"

#define LEFT_CHANNEL 2
#define RIGHT_CHANNEL 3

// Get the coordinates of the left hand
int locateLeftX(){
    camera_update();
    return get_object_center_x(LEFT_CHANNEL, 0);
}

// Get the coordinates of the left hand
int locateLeftY(){
    camera_update();
    return get_object_center_y(LEFT_CHANNEL, 0);
}

// Get the coordinates of the right hand
int locateRightX(){
    camera_update();
    return get_object_center_x(RIGHT_CHANNEL, 0);
}

// Get the coordinates of the right hand
int locateRightY(){
    camera_update();
    return get_object_center_y(RIGHT_CHANNEL, 0);
}