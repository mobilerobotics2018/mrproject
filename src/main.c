/*
 * Jack Price
 * Zach Wong
 * Mobile Robotics I
 * Final Project
 */

#include <stdio.h>
#include "pilot.h"
#include "navigator.h"
#include "init.h"
#include "planner.h"

int main() {
    int goalReached = 0;
    struct DLNode* activeWaypoint;

    // Run initialization routines for robot and camera
    init();

    // Begin planning cycles
    // Only running one cycle since camera tilt and replanning is untested
	while(!goalReached) {
		// get the route to follow
		activeWaypoint = planner();

		while (!goalReached) {
			//check if route modified and change
			updateRoute(activeWaypoint);

			// move to the active waypoint in the route
			if(moveWaypoint(activeWaypoint)){
			    // If the reached waypoint is the goal, finish execution
                if (activeWaypoint->type){
                    goalReached = 1;
                }

                // if active waypoint has been reached, queue up the next one
			    activeWaypoint = activeWaypoint->next;
			}
		}

		// Robot moved to top of frame, move camera then re-plan for next route
        // Not implemented from lack of testing
        // putBotInFrame(0);

	}

    return 0;
}