// planner.c
// functions for planning the route of the robot

#include <malloc.h>
#include "planner.h"
#include "machine.h"

#define WAYPOINT_CHANNEL 0
#define GOAL_CHANNEL 1

struct DLNode* planner(){
    // Get channels with waypoints and goal
    int waypointCount, i, end = 1;
    struct DLNode* top, *route, *waypoint;

    top = malloc(sizeof(struct DLNode*));
    top->type=0;
    top->next=NULL;
    top->prev=NULL;
    top->x=0;
    top->y = 0;

    route = top;

    printf("Beginning planning stage.\n");

    if(camera_update()){
        // get channel 0 (waypoints) objects
        waypointCount = get_object_count(WAYPOINT_CHANNEL);
        for (i = 0; i < waypointCount; i++){
            end = 1;
            waypoint = malloc(sizeof(struct DLNode*));
            waypoint->x = get_object_center_x(WAYPOINT_CHANNEL, i);
            waypoint->y = get_object_center_y(WAYPOINT_CHANNEL, i);
            waypoint->type = 0;

            // Insert the new waypoint by height on the wall (small list)
            route = top;
            while (route->next != NULL){
                if (waypoint->y > route->y){
                    route = route->next;
                }
                else{
                    waypoint->next = route;
                    waypoint->prev = route->prev;
                    waypoint->prev->next = waypoint;
                    route->prev = waypoint;
                    end = 0;
                    break;
                }
            }
            if (end) {
                waypoint->next = route->next;
                waypoint->prev = route;
                waypoint->next->prev = waypoint;
                route->next = waypoint;
            }
        }

        // Get channel 1 (goal) objects
        // Add goal marker to end of route list
        while(route->next != NULL){
            route = route->next;
        }

        waypoint = malloc(sizeof(struct DLNode*));

        waypoint->x = get_object_center_x(GOAL_CHANNEL, 0);
        waypoint->y = get_object_center_y(GOAL_CHANNEL, 1);
        waypoint->type = 1;

        waypoint->prev = route;
        waypoint->next = NULL;
        route ->next = waypoint;
    }

    printf("Planing finished.\n");
}