// pilot.c
// used to directly control robot movement

#include <include/planner.h>
#include <math.h>
#include "pilot.h"
#include "motors.h"
#include "tracking.h"
#include "emag.h"

#define DEFAULT_SPEED 200
#define LEFT_ARM -1
#define RIGHT_ARM 1

int moveWaypoint(struct DLNode* activeWaypoint){
    int x_R, y_R, slope, offset, y_left, y_right, reached = 0, side;
    struct line movementLine;

    // get x_r y_r from here
    y_left  = locateLeftY();
    y_right = locateRightY();

    while(!reached) {
        // Select which arm to swing, swing the lower arm up
        if (y_left < y_right) {
            x_R = locateLeftX();
            y_R = locateLeftY();
            side = LEFT_ARM;
        } else {
            x_R = locateRightX();
            y_R = locateRightY();
            side = RIGHT_ARM;
        }

        // Create line of movement
        _makeLine(x_R, y_R, activeWaypoint->x, activeWaypoint->y, &movementLine);

        reached = _waypointReached(x_R, y_R, activeWaypoint->x, activeWaypoint->y, movementLine);

        _moveIntersect(movementLine, side);
    }

    return 0;
}

void _moveLeftIntersect(int slope, int offset){
    float distance;
    if((distance = _onLine(locateLeftX(), locateLeftY(), slope, offset))< 0){
        // when line has been reached, stop and activate the magnet
        motors_leftFreeze();
        emag_leftOn();
        return;
    }
    // line has not been reached, keep moving
    emag_leftOff();

    motors_leftSpeed(DEFAULT_SPEED);
    motors_rightSpeed(-DEFAULT_SPEED);
}

void _moveRightIntersect(int slope, int offset){
    float distance;
    if((distance = _onLine(locateRightX(), locateRightY(), slope, offset))< 0){
        // when line has been reached, stop and activate the magnet
        motors_rightFreeze();
        emag_rightOn();
        return;
    }
    // line has not been reached, keep moving
    emag_rightOff();

    motors_leftSpeed(-DEFAULT_SPEED);
    motors_rightSpeed(DEFAULT_SPEED);
}

void _moveIntersect(struct line intersectingLine, int upperArm){
    if (upperArm < 0){
        _moveLeftIntersect(intersectingLine.slope, intersectingLine.intercept);
    }
    else if (upperArm > 0){
        _moveRightIntersect(intersectingLine.slope, intersectingLine.intercept);
    }
    else{
        if (intersectingLine.slope > 0){
            _moveRightIntersect(intersectingLine.slope, intersectingLine.intercept);
        }
        else{
            _moveLeftIntersect(intersectingLine.slope, intersectingLine.intercept);
        }
    }
}

void _makeLine(int x0, int y0, int x1, int y1, struct line * movementLine){
    movementLine->slope = (float)(y1 - y0) / (float)(x1 - x0);
    movementLine->intercept = y1 - (movementLine->slope * x1);
}

double getDistance(double x1, double x2, double y1, double y2) {
    return sqrt(((pow((x2 - x1), 2) + pow((y2 - y1), 2))));
}

int _onLine(int x, int y, float slope, float offset){
    return (x * slope + offset) == y;
}

int _waypointReached(int x_R, int y_R, int x_W, int y_W, struct line intersectingLine){
    // waypoint reached if upper hand is above waypoint and intersects with movement line
    return y_R > y_W && _onLine(x_R, y_R, intersectingLine.slope, intersectingLine.intercept);
}

