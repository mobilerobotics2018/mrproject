/*
Jack Price
Zachary Wong
Mobile Robotics I
Lab 5
*/

#include "common.h"
#include "machine.h"
#include "motors.h"

const int DEFAULT_SPEED = 750;
const int LEFT_MOTOR = 0;
const int RIGHT_MOTOR = 1;

void motors_bothDefault(){
    motors_bothSpeed(DEFAULT_SPEED);
}

void motors_inPlaceDefault(int direction){
    motors_inPlaceSpeed(direction, DEFAULT_SPEED);
}

void motors_stopLeft(){
    off(LEFT_MOTOR);
}

void motors_stopRight(){
    off(RIGHT_MOTOR);
}

void motors_stopBoth(){
    alloff();
}

void motors_leftDefault(){
    motors_leftSpeed(DEFAULT_SPEED);
}

void motors_rightDefault(){
    motors_rightSpeed(DEFAULT_SPEED);
}

void motors_bothBack(){
    motors_leftBack();
    motors_rightBack();
}

void motors_leftBack(){
    motors_leftSpeed(-DEFAULT_SPEED);
}

void motors_rightBack(){
    motors_rightSpeed(-DEFAULT_SPEED);
}

void motors_leftSpeed(int speed){
    move_at_velocity(LEFT_MOTOR, speed);
}

void motors_rightSpeed(int speed){
    move_at_velocity(RIGHT_MOTOR, speed);
}

void motors_inPlaceSpeed(int direction, int speed){
    if (direction == LEFT){
        motors_leftSpeed(speed);
        motors_rightSpeed(-speed);
    }
    else if (direction == RIGHT){
        motors_leftSpeed(-speed);
        motors_rightSpeed(speed);
    }
}

void motors_bothSpeed(int speed){
    motors_leftSpeed(speed);
    motors_rightSpeed(speed);
}

void motors_inPlaceAngle(int angle){
    clear_motor_position_counter(LEFT_MOTOR);
    clear_motor_position_counter(RIGHT_MOTOR);
	
    move_to_position(LEFT_MOTOR, 500, 500);
    move_to_position(LEFT_MOTOR, -500, 500);
    
    block_motor_done(LEFT_MOTOR);
    block_motor_done(RIGHT_MOTOR);
    
}
void rotate_45_left(){
    motors_inPlaceSpeed(LEFT, 500);
    msleep(5000);
    motors_stopBoth();
}

void rotate_45_right(){
    motors_inPlaceSpeed(LEFT, 500);
    msleep(5000);
    motors_stopBoth();
}

void rotate_90_left(){
    motors_inPlaceSpeed(LEFT, 500);
    msleep(5000);
    msleep(5000);
    motors_stopBoth();
}


void rotate_90_right(){
    motors_inPlaceSpeed(RIGHT, 500);
    msleep(5000);
    msleep(5000);
    motors_stopBoth();
}

void rotate_180(){
    motors_inPlaceSpeed(RIGHT, 500);
    msleep(5000);
    msleep(5000);
    msleep(5000);
    msleep(5000);
    motors_stopBoth();
}

void motors_leftFreeze(){
    _motor_freeze(LEFT_MOTOR);
}

void motors_rightFreeze(){
    _motor_freeze(RIGHT_MOTOR);
}

void _motor_freeze(int motor){
    freeze(motor);
}