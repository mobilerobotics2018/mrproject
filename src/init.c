// init.c
// contains functions to initialize the robot and camera

#include "init.h"
#include "machine.h"
#include "emag.h"

void init(){
    int aPressed = 0;

    // Set digital ports to output
    set_digital_output(0, 1);
    set_digital_output(1, 1);

    // Give start prompt and wait to press A
    printf("Place robot at bottom of wall and press A button to start\n");
    while(!aPressed){
        aPressed = a_button();
        msleep(10);
    }

    // Turn on electromagnets, attach to wall
    emag_leftOn();
    emag_rightOn();

    // Turn on camera
    camera_open();

    printf("Starting climb in 3 seconds.\n");

    msleep(3000);
}
