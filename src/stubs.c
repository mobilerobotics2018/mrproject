/*
Jack Price
Zachary Wong
Mobile Robotics I
Lab 4
*/

/* Used to provide wallaby function definitions for offline development
 *
 * THe function definitions were copied in from the libwallaby source files
 * found at https://github.com/kipr/libwallaby.  They are not implemented, and only used
 * for compiling when not developing on the wallaby.
 */

#include "stubs.h"
#include <stdio.h>

void alloff(){perror("Stub function not implemented, comment out PC_DEV, uncomment WALLABY_DEV in machine.h");}

void off(int motor){perror("Stub function not implemented, comment out PC_DEV, uncomment WALLABY_DEV in machine.h");}

int move_at_velocity(int motor, int velocity){perror("Stub function not implemented, comment out PC_DEV, uncomment WALLABY_DEV in machine.h");return 0;}

int analog(int port){perror("Stub function not implemented, comment out PC_DEV, uncomment WALLABY_DEV in machine.h");return 0;}

int digital(int port){perror("Stub function not implemented, comment out PC_DEV, uncomment WALLABY_DEV in machine.h");return 0;}

void set_a_button_text(const char * text){perror("Stub function not implemented, comment out PC_DEV, uncomment WALLABY_DEV in machine.h");}

void set_b_button_text(const char * text){perror("Stub function not implemented, comment out PC_DEV, uncomment WALLABY_DEV in machine.h");}

int a_button(){perror("Stub function not implemented, comment out PC_DEV, uncomment WALLABY_DEV in machine.h");return 0;}

int b_button(){perror("Stub function not implemented, comment out PC_DEV, uncomment WALLABY_DEV in machine.h");return 0;}

void clear_motor_position_counter(int motor){perror("Stub function not implemented, comment out PC_DEV, uncomment WALLABY_DEV in machine.h");}

int move_to_position(int motor, int speed, int goal_pos){perror("Stub function not implemented, comment out PC_DEV, uncomment WALLABY_DEV in machine.h");return 0;}

void block_motor_done(int motor){perror("Stub function not implemented, comment out PC_DEV, uncomment WALLABY_DEV in machine.h");}

int camera_open(){perror("Stub function not implemented, comment out PC_DEV, uncomment WALLABY_DEV in machine.h");}

int camera_open_black(){perror("Stub function not implemented, comment out PC_DEV, uncomment WALLABY_DEV in machine.h");}

int get_object_count(int channel){perror("Stub function not implemented, comment out PC_DEV, uncomment WALLABY_DEV in machine.h");}

int get_object_center_x(int channel, int object){perror("Stub function not implemented, comment out PC_DEV, uncomment WALLABY_DEV in machine.h");}

int get_object_center_y(int channel, int object){perror("Stub function not implemented, comment out PC_DEV, uncomment WALLABY_DEV in machine.h");}

int get_object_bbox_width(int channel, int object){perror("Stub function not implemented, comment out PC_DEV, uncomment WALLABY_DEV in machine.h");}

double get_object_confidence(int channel, int object){perror("Stub function not implemented, comment out PC_DEV, uncomment WALLABY_DEV in machine.h");return 0.0;}

int camera_update(void){perror("Stub function not implemented, comment out PC_DEV, uncomment WALLABY_DEV in machine.h");return 0;}

void camera_close(){perror("Stub function not implemented, comment out PC_DEV, uncomment WALLABY_DEV in machine.h");}

void set_digital_output(int port, int out){perror("Stub function not implemented, comment out PC_DEV, uncomment WALLABY_DEV in machine.h");}

void set_digital_value(int port, int value){perror("Stub function not implemented, comment out PC_DEV, uncomment WALLABY_DEV in machine.h");}

int freeze(int motor){perror("Stub function not implemented, comment out PC_DEV, uncomment WALLABY_DEV in machine.h");return 0;}

void msleep(int ms){perror("Stub function not implemented, comment out PC_DEV, uncomment WALLABY_DEV in machine.h");}

void enable_servo(int port){perror("Stub function not implemented, comment out PC_DEV, uncomment WALLABY_DEV in machine.h");}

void disable_servo(int port){perror("Stub function not implemented, comment out PC_DEV, uncomment WALLABY_DEV in machine.h");}

void set_servo_position(int port, int position){perror("Stub function not implemented, comment out PC_DEV, uncomment WALLABY_DEV in machine.h");}