cmake_minimum_required(VERSION 3.10)
project(mrproject C)

set(CMAKE_C_STANDARD 99)

include_directories(. ./include ./src)

add_executable(mrproject src/main.c src/common.c include/common.h src/emag.c include/emag.h src/init.c include/init.h include/machine.h src/motors.c include/motors.h src/navigator.c include/navigator.h src/pilot.c include/pilot.h src/planner.c include/planner.h src/stubs.c include/stubs.h src/tracking.c include/tracking.h)