/*
Jack Price
Zachary Wong
Mobile Robotics I
Lab 6
*/

// Use to choose which machine this is running on

#ifndef LAB5_MACHINE_H
#define LAB5_MACHINE_H

// !!! Comment out for deployment to Wallaby !!!
#define PC_DEV
// !!! Uncomment for deployment to wallaby !!!
//#define WALLABY_DEV

#ifdef PC_DEV
#include "stubs.h"
#else
#ifdef WALLABY_DEV
#include <kipr/botball.h>
#endif
#endif


#include "common.h"

#endif //LAB5_MACHINE_H
