// init.h
// contains functions to initialize the robot and camera

#ifndef WATCHER_INIT_H
#define WATCHER_INIT_H

void init();

#endif //WATCHER_INIT_H
