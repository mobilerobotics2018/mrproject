/*
Jack Price
Zachary Wong
Mobile Robotics I
Lab 4
*/



/* Used to provide wallaby function prototypes for offline development
 *
 * THe function prototypes were copied in from the libwallaby source files
 * found at https://github.com/kipr/libwallaby.  They are not implemented, and only used
 * for compiling when not developing on the wallaby.
 */

#ifndef LAB3_STUBS_H
#define LAB3_STUBS_H

/*!
 * Gets the 12-bit analog value of a port.
 * \param[in] port A value between 0 and 5 specifying the sensor to read from.
 * \return The latest 12-bit value of the port (a value in the range 0 to 4095).
 * \see analog12
 * \ingroup analog
 */
int analog(int port);


/*!
 * Gets the 8-bit analog value of a port.
 * \param[in] port A value between 0 and 5 specifying the sensor to read from.
 * \return The latest 8-bit value of the port (a value in the range 0 to 255).
 * \see analog
 * \ingroup analog
 */
int analog8(int port);


/*!
 * Gets the 10-bit analog value of a port.
 * \param[in] port A value between 0 and 5 specifying the sensor to read from.
 * \return The latest 10-bit value of the port (a value in the range 0 to 1023).
 * \see analog
 * \ingroup analog
 */
int analog10(int port);


/*!
 * Gets the 12-bit analog value of a port.
 * \param[in] port A value between 0 and 5 specifying the sensor to read from.
 * \return The latest 12-bit value of the port (a value in the range 0 to 4095).
 * \see analog
 * \ingroup sensor
 */
int analog12(int port);


/*!
 * Gets the 10-bit analog value of an ET sensor on the given port.
 * \param[in] port A value between 0 and 7 specifying the ET sensor to read from.
 * \return The latest 10-bit value of the port (a value in the range 0 to 1023).
 * \see analog
 * \ingroup analog
 * \deprecated defaulting to analog() on the Wallaby
 */
int analog_et(int port);


/*!
 * Sets analog pullup status for one port.
 * \param[in] port A value between 0 and 5 specifying the analog sensor to read from.
 * \param[in] pullup A value of 0 (inactive) or 1 (active).
 * \see analog
 * \ingroup analog
 * \deprecated no effect on the Wallaby
 */

void set_analog_pullup(int port, int pullup);

/*!
 * Gets the analog pullup status for one portt.
 * \param[in] port A value between 0 and 7 specifying the analog sensor to read from.
 * \return The status of the analog pullup on the specified port
 * \see analog
 * \ingroup analog
 * \deprecated no effect on the Wallaby
 */
int get_analog_pullup(int port);

/*!
 * \see get_digital_value
 * \ingroup digital
 */
int digital(int port);

/*!
 * Sets the value of the digital port in output mode.
 * \see get_digital_value
 * \ingroup digital
 */
void set_digital_value(int port, int value);


/*!
 * Gets the current value of the digital port.
 * \return 1 if the switch is closed, 0 if the switch is open
 * \see set_digital_value
 * \ingroup digital
 */
int get_digital_value(int port);

/*!
 * Sets the digital mode.
 * \param[in] port The port to modify.
 * \param[in] out 1 for output mode, 0 for input mode.
 * \ingroup digital
 */
void set_digital_output(int port, int out);

/*!
 * Gets the current digital mode
 * \return 1 for output mode, 0 for input mode
 * \see set_digital_value
 * \ingroup digital
 */
int get_digital_output(int port);

/*!
 * Gets the current digital pullup state
 * \return 1 for active, 0 for inactive
 * \deprecated not applicable on the Wallaby
 * \ingroup digital
 */
int get_digital_pullup(int port);

/*!
 * Sets the current digital pullup state
 * \param[in] port The port to modify
 * \param[in] pullup The pullup state 1: active  0: inactive
 * \deprecated not applicable on the Wallaby
 * \ingroup digital
 */
void set_digital_pullup(int port, int pullup);

/*!
 * \brief Gets the current motor position
 * \param[in] motor The motor port.
 * \ingroup motor
 * \see gmpc
 */
int get_motor_position_counter(int motor);

/*!
 * \brief Gets the current motor position
 * \param[in] motor The motor port.
 * \ingroup motor
 * \see get_motor_position_counter
 */
int gmpc(int motor);


/*!
 * \brief Clears the motor position counter
 * \param[in] motor The motor port.
 * \ingroup motor
 * \see cmpc
 */
void clear_motor_position_counter(int motor);


/*!
 * \brief Clears the motor position counter
 * \param[in] motor The motor port.
 * \ingroup motor
 * \see clear_motor_position_counter
 */
void cmpc(int motor);



/*!
 * \brief Set a goal velocity in ticks per second.
 * \detailed The range is -1500 to 1500, though motor position accuracy may be decreased outside of -1000 to 1000
 * \param[in] motor The motor port.
 * \param[in] velocity The goal velocity in -1500 to 1500 ticks / second
 * \ingroup motor
 * \see mav
 */
int move_at_velocity(int motor, int velocity);

/*!
 * \brief Set a goal velocity in ticks per second
 * \param[in] motor The motor port.
 * \param[in] velocity The goal velocity in -1500 to 1500 ticks / second
 * \ingroup motor
 * \see move_at_velocity
 */
int mav(int motor, int velocity);


/*!
 * \brief Set a goal position (in ticks) for the motor to move to.
 * \detailed There are approximately 1500 ticks per motor revolution.
 * \detailed This function is more accurate if speeds between -1000 and 1000 are used.
 * \param[in] motor The motor port.
 * \param[in] speed The speed to move at, between -1500 and 1500 ticks / second
 * \param[in] goal_pos The position to move to (in ticks)
 * \ingroup motor
 * \see mtp
 */
int move_to_position(int motor, int speed, int goal_pos);


/*!
 * \brief Set a goal position (in ticks) for the motor to move to
 * \param[in] motor The motor port.
 * \param[in] speed The speed to move at, between -1500 and 1500 ticks / second
 * \param[in] goal_pos The position to move to (in ticks)
 * \ingroup motor
 * \see move_to_position
 */
int mtp(int motor, int speed, int goal_pos);

/*!
 * \brief Set a goal position (in ticks) for the motor to move to, relative to the current position
 * \param[in] motor The motor port.
 * \param[in] speed The speed to move at, between -1500 and 1500 ticks / second
 * \param[in] delta_pos The position to move to (in ticks) given the current position
 * \ingroup motor
 * \see mrp
 */
int move_relative_position(int motor, int speed, int delta_pos);

/*!
 * \brief Set a goal position (in ticks) for the motor to move to, relative to the current position
 * \param[in] motor The motor port.
 * \param[in] speed The speed to move at, between -1500 and 1500 ticks / second
 * \param[in] delta_pos The position to move to (in ticks) given the current position
 * \ingroup motor
 * \see move_relative_position
 */
int mrp(int motor, int speed, int delta_pos);

/*!
 * \brief Set the motor PID gains, represented as fractions.
 * \param[in] motor The motor port.
 * \param[in] p The P (proportional) gain numerator
 * \param[in] i The I (integral) gain numerator
 * \param[in] d The D (derivative) gain numerator
 * \param[in] pd The P (proportional) gain denominator
 * \param[in] id The I (integral) gain denominator
 * \param[in] dd The D (derivative) gain denominator
 * \ingroup motor
 */
void set_pid_gains(int motor, short p, short i, short d, short pd, short id, short dd);


/*!
 * \brief Set the motor PID gains, represented as fractions.
 * \param[out] motor The motor port.
 * \param[out] p The P (proportional) gain numerator
 * \param[out] i The I (integral) gain numerator
 * \param[out] d The D (derivative) gain numerator
 * \param[out] pd The P (proportional) gain denominator
 * \param[out] id The I (integral) gain denominator
 * \param[out] dd The D (derivative) gain denominator
 * \ingroup motor
 */
void get_pid_gains(int motor, short * p, short * i, short * d, short * pd, short * id, short * dd);

/*!
 * \brief Active braking to stop a motor
 * \param[in] motor The motor port.
 * \ingroup motor
 */
int freeze(int motor);

/*!
 * \brief Check if the motor has reached it's goal
 * \param[in] motor The motor port.
 * \ingroup motor
 * \returns 1: at goal   0: not at goal
 */
int get_motor_done(int motor);

/*!
 * \brief Wait until the motor is at it's goal
 * \param[in] motor The motor port.
 * \see bmd
 * \ingroup motor
 */
void block_motor_done(int motor);

/*!
 * \brief Wait until the motor is at it's goal
 * \param[in] motor The motor port.
 * \see block_motor_done
 * \ingroup motor
 */
void bmd(int motor);

/*!
 * \brief Set the motor pwm (percent power) command
 * \param[in] motor The motor port.
 * \param[in] pwm A new motor pwm command between 0 and 100
 * \ingroup motor
 */
int setpwm(int motor, int pwm);

/*!
 * \brief Get the current motor pwm command
 * \param[in] motor The motor port.
 * \ingroup motor
 */
int getpwm(int motor);


/*!
 * \brief Moves the given motor forward at full power
 * \param motor the motor's port.
 * \ingroup motor
 */
void fd(int motor);


/*!
 * \brief Moves the given motor backward at full power
 * \param motor the motor's port.
 * \ingroup motor
 */
void bk(int motor);


/*!
 * \brief Moves a motor at a percent velocity.
 *
 * \param[in] motor The motor port.
 * \param[in] percent The percent of the motors velocity, between -100 and 100.
 *
 * \ingroup motor
 */
void motor(int motor, int percent);

/*!
 * \brief Moves a motor at a percent power.
 *
 * \param[in] motor the motor port.
 * \param[in] percent The power of the motor, between -100 and 100.
 *
 * \ingroup motor
 */
void motor_power(int motor, int percent);


/*!
 * \brief Turns the specified motor off.
 * \param motor the motor's port.
 * \ingroup motor
 */
void off(int motor);

/*!
 * \brief Turns all motors off.
 * \ingroup motor
 * \see ao
 */
void alloff();

/*!
 * \brief Turns all motors off.
 * \ingroup motor
 * \see alloff
 */
void ao();

/*!
 * Updates the A button's text.
 * \param text The text to display. Limit of 16 characters.
 * \ingroup button
 * \deprecated not planned for the Wallaby
 */
void set_a_button_text(const char * text);

/*!
 * Updates the B button's text.
 * \param text The text to display. Limit of 16 characters.
 * \ingroup button
 * \deprecated not planned for the Wallaby
 */
void set_b_button_text(const char * text);

/*!
 * Gets the A button's state (pressed or not pressed.)
 * \return 1 for pressed, 0 for not pressed
 * \see a_button_clicked
 * \ingroup button
 * \note Not yet implemented
 */
int a_button();

/*!
 * Gets the B button's state (pressed or not pressed.)
 * \return 1 for pressed, 0 for not pressed
 * \see a_button_clicked
 * \ingroup button
 * \note Not yet implemented
 */
int b_button();

/*!
 * \brief Clears the motor position counter
 * \param[in] motor The motor port.
 * \ingroup motor
 * \see cmpc
 */
void clear_motor_position_counter(int motor);

/*!
 * \brief Set a goal position (in ticks) for the motor to move to.
 * \detailed There are approximately 1500 ticks per motor revolution.
 * \detailed This function is more accurate if speeds between -1000 and 1000 are used.
 * \param[in] motor The motor port.
 * \param[in] speed The speed to move at, between -1500 and 1500 ticks / second
 * \param[in] goal_pos The position to move to (in ticks)
 * \ingroup motor
 * \see mtp
 */
int move_to_position(int motor, int speed, int goal_pos);

/*!
 * \brief Wait until the motor is at it's goal
 * \param[in] motor The motor port.
 * \see bmd
 * \ingroup motor
 */
void block_motor_done(int motor);


/**
 * Opens the default system camera for use at LOW_RES (160x120).
 * \return 1 on success, 0 on failure
 * \see camera_open_at_res
 * \see camera_open_device
 * \see camera_close
 * \ingroup camera
 */
int camera_open();

/**
 * Opens the default system camera for use at LOW_RES (160x120).
 * This will improve frame rates for the black Logitech camera
 * \return 1 on success, 0 on failure
 * \see camera_open_at_res
 * \see camera_open_device
 * \see camera_close
 * \ingroup camera
 */
int camera_open_black();

/**
 * \param channel The channel to scan for objects.
 * \note Objects are sorted by area, largest first.
 * \return Number of objects in the given channel, -1 if channel doesn't exist.
 * \see get_channel_count
 * \ingroup camera
 */
int get_object_count(int channel);


/**
 * \return The (x, y) center of the given object on the given channel.
 * \ingroup camera
 */
int get_object_center_x(int channel, int object);

int get_object_center_y(int channel, int object);

int get_object_bbox_width(int channel, int object);

/**
 * \return The confidence, between 0.0 and 1.0, that given object on the given channel is significant.
 * If the channel or object doesn't exist, 0.0 is returned.
 * \ingroup camera
 */
double get_object_confidence(int channel, int object);

/**
 * Pulls a new image from the camera for processing.
 * \return 1 on success, 0 on failure.
 * \ingroup camera
 */
int camera_update(void);

/**
 * Cleanup the current camera instance.
 * \see camera_open
 * \see camera_open_at_res
 * \see camera_open_device
 * \ingroup camera
 */
void camera_close();

void set_digital_output(int port, int out);

void set_digital_value(int port, int value);

/*!
 * \brief Active braking to stop a motor
 * \param[in] motor The motor port.
 * \ingroup motor
 */
int freeze(int motor);

void msleep(int ms);

/**
 * \brief Enable a specific servo
 * \param[in] port The port, between 0 and 3, to enable
 * \ingroup servo
 */
void enable_servo(int port);

/**
 * \brief Disable a specific servo
 * \param[in] port The port, between 0 and 3, to disable
 * \ingroup servo
 */
void disable_servo(int port);

/*!
 * \brief Set a new servo goal position
 * \param servo The port of the servo
 * \param position The new servo position, between 0 and 2047
 *
 * \note Even though the servos have a _theoretical_ range between 0 and 2047,
 * the _actual_ range is often less. Setting the servo to a position that it cannot physically
 * reach will cause the servo to audibly strain and will consume battery very quickly.
 * \ingroup servo
 */
void set_servo_position(int port, int position);

#endif //LAB3_STUBS_H
