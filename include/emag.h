// emag.h
// Contains functions to control the electro magnets

#ifndef WATCHER_EMAG_H
#define WATCHER_EMAG_H

void emag_leftOn();

void emag_leftOff();

void emag_rightOn();

void emag_rightOff();

/*****************************
 * Helper Functions (private)
 *****************************/

// Turn the magnet in the given digital slot on
void _turnOn(int magnet);

// Turn the magnet in the given digital slot off
void _turnOff(int magnet);

#endif //WATCHER_EMAG_H
