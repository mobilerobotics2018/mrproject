// tracking.h
// Provides functionality to locate the robot and its relative position

#ifndef WATCHER_TRACKING_H
#define WATCHER_TRACKING_H

// Get the coordinates of the left hand
int locateLeftX();
int locateLeftY();

// Get the coordinates of the right hand
int locateRightX();
int locateRightY();


#endif //WATCHER_TRACKING_H
