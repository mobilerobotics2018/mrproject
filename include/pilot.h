// pilot.h
// Used to directly control robot movement

/*
 * The robot moves by two motors connected on either end of an arm, with an electromagnet
 * on each motor that latches onto the wall in turn, making a hand-over-hand kind of movement up a steel wall.
 * Each motor has a default direction it moves, to prevent wires tangling
 * Each end of the arm is tracked by vision to determine position
 */

#ifndef WATCHER_PILOT_H
#define WATCHER_PILOT_H

typedef struct line{
    float slope;
    float intercept;
};

// Move the robot until it passes a given waypoint. returns true if waypoint reached
int moveWaypoint(struct DLNode* activeWaypoint);

/*****************************
 * Helper Functions
 *****************************/

// Move the left motor until it intersects with a line
void _moveLeftIntersect(int slope, int offset);

// Move the right motor until it intersects with a line
void _moveRightIntersect(int slope, int offset);

// Move the robot until it intersects with a line
void _moveIntersect(struct line intersectingLine, int upperArm);

/**********************************
 * Line helper functions
 **********************************/

// Create a line of slope intersect form
void _makeLine(int x0, int y0, int x1, int y1, struct line * movementLine);

// Check if a point is on a line in slope-intersect form.
int _onLine(int x, int y, float slope, float offset);

// Identify whether a point qualifies as completing a waypoint
int _waypointReached(int x_R, int y_R, int x_W, int y_W, struct line intersectingLine);

double getDistance(double x1, double x2, double y1, double y2);

#endif //WATCHER_PILOT_H
