// navigator.h
// Module used to check if any part of the route has changed

#ifndef WATCHER_NAVIGATOR_H
#define WATCHER_NAVIGATOR_H

#include "planner.h"

// Update coordinates of waypoint nodes if they are noticed to have changed from original measurement
// Planned for implementation upon successful execution of hierarchical planning design
void updateRoute(struct DLNode*);

#endif //WATCHER_NAVIGATOR_H
