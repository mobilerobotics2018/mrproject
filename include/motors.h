

/*
Jack Price
Zachary Wong
Mobile Robotics I
Lab 5
*/

#ifndef LAB5_MOTORS_H
#define LAB5_MOTORS_H

#define LEFT 0
#define RIGHT 1

// Move both motors forward at default speed.
void motors_bothDefault();

// Turn in place at default speed. directions are LEFT, RIGHT
void motors_inPlaceDefault(int direction);

// Passively stop the left motor
void motors_stopLeft();

// Passively stop the right motor.
void motors_stopRight();

// Passively stop both motors.
void motors_stopBoth();

// Move left motor forward at default speed.
void motors_leftDefault();

// Move right motor forward at default speed.
void motors_rightDefault();

// Move both motors backward at default speed.
void motors_bothBack();

// Move left motor backwards at default speed
void motors_leftBack();

// Move right mottor backward ar default speed.
void motors_rightBack();

// Move left motor at specified speed (-1000 -> 1000).
void motors_leftSpeed(int speed);

// Move right motor at specified speed (-1000 -> 1000).
void motors_rightSpeed(int speed);

// Turn in place at given direction (LEFT, RIGHT) and speed (-1000 -> 1000)
void motors_inPlaceSpeed(int direction, int speed);

// Move both motors at the given speed (-1000 -> 1000)
void motors_bothSpeed(int speed);

// Pivot the in place robot to the specified angle (-180 -> +180)
void motors_inPlaceAngle(int angle);

// Rotate robot 45 degrees left
void rotate_45_left();

// Rotate robot 45 degrees right
void rotate_45_right();

// Rotate robot 90 degrees left
void rotate_90_left();

// Rotate robot 90 degrees right
void rotate_90_right();

// Rotate robot 180 degrees
void rotate_180();

void motors_leftFreeze();

void motors_rightFreeze();

void _motor_freeze(int motor);

#endif //LAB5_MOTORS_H

