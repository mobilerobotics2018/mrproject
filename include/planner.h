// planner.h
// functions for planning the route of the robot

#ifndef WATCHER_PLANNER_H
#define WATCHER_PLANNER_H

/* Node of a doubly linked list */
typedef struct DLNode
{
    int x; // x coordinate of the waypoint
    int y; // y coordinate of the waypoint
    int type; // 0 is regular waypoint, 1 is goal waypoint
    struct DLNode *next;
    struct DLNode *prev;
}list_head, *currentWaypoint;

// Function which identifies waypoints and passes them to navigator.
//  Returns pointer to first waypoint in DL list which makes the route
struct DLNode* planner();

// Tilt the camera up so that the robot is at the bottom of the frame to initiate a new planning cycle
// (not implemented)
void putBotInFrame(int servo);

#endif //WATCHER_PLANNER_H
