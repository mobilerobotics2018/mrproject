/*
Jack Price
Zachary Wong
Mobile Robotics I
Lab 6
*/

#ifndef COMMON_H
#define COMMON_H

#include "machine.h"
#include <stdio.h>
#include <unistd.h>
#include <time.h>

#define TRUE 1
#define FALSE 0

#endif